json.array!(@api_v1_users) do |api_v1_user|
  json.extract! api_v1_user,  :user, :name, :last_name, :email, :company, :phone, :latitude, :longitude
  
end
